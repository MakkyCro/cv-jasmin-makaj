import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    name: '',
    path: '/:lang?',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', redirect: 'work-experience' },
      { name: 'introduction', path: 'introduction', component: () => import('pages/IntroductionPage.vue') },
      { name: 'work-experience', path: 'work-experience', component: () => import('pages/WorkExperiencePage.vue') },
      { name: 'projects', path: 'projects', component: () => import('pages/ProjectsPage.vue') },
      { name: 'education', path: 'education', component: () => import('pages/EducationPage.vue') },
      { path: '/:catchAll(.*)*', redirect: 'work-experience' },
    ],
  },

  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
