import { route } from 'quasar/wrappers';
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory,
} from 'vue-router';
import routes from './routes';
import { useLocaleStore } from 'src/stores/store';
import { Platform } from 'quasar'

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const localeStore = useLocaleStore();

  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory);

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(
      process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE
    ),
  });

  Router.beforeEach((to, from, next) => {

    if (!Platform.is.mobile && to.name == 'introduction') {
      next({ path: '/work-experience' });
    }

    if (!/\/[A-z]{2,3}(\/|$|#)/.test(to.fullPath)) {
      const tmp = from.path.match(/\/([a-z]{2,3})(\/|$)/);
      let fromLocale = localeStore.getLocale();
      if (tmp && tmp.length > 1) {
        fromLocale = tmp[1];
      }
      const path = '/' + fromLocale + (to.path.startsWith('/') ? '' : '/') + to.fullPath;

      next({ path, query: to.query });
      return;
    }
    next();
  });

  return Router;
});
