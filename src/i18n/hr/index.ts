export default {
  untilToday: 'Do Danas',
  year: 'Godina',
  years: 'Godine',
  month: 'Mjesec',
  months: 'Mjeseca',
  website: 'Web stranice',
  webdesign: 'Web dizajn',
  games: 'Igre',
  misc: 'Ostalo',
  intro: {
    title: 'Uvod',
    text: 'Radio sam s širokim rasponom tehnologija, no uvijek sam motiviran naučiti više i koristiti najnoviju dostupnu tehnologiju. Uz izvrsne komunikacijske vještine, također imam puno iskustva u radu u malim timovima.'
  },
  languages: {
    title: 'Jezici',
    croatian: 'Hrvatski',
    german: 'Njemački',
    english: 'Engleski',
  },
  contact: {
    title: 'Kontakt'
  },
  nav: {
    introduction: 'O Meni',
    workExperience: 'Radno Iskustvo',
    projects: 'Moji Projekti',
    education: 'Moje Obrazovanje',
    selectLanguage: 'Odaberi Jezik',
    openInGitlab: 'Prikaži Gitlab projekt',
    showVideo: 'Prikaži Video',
    openWebsite: 'Prikaži Projekt'
  },
  education: {
    highschool: {
      title: 'Tehničar za mehatroniku',
      location: 'Elektrostrojatska škola,\nVaraždin (Hrvatska)',
      content: '• Izrada softvera u C# programskom jeziku, programiranje mikroupravljača, LOGO uređaja (Logo! Soft) i PLC uređaja proizvodne trake za automatizaciju procesa u industriji (Siemens SIMATIC), programiranje Arduino uređaja, robotika, programiranje robotske ruke, obnovljivi izvori energije, hidraulika i pneumatika, CNC strojevi, 3D modeliranje (Solid Works), strojarstvo i kontrola kvalitete, elektrotehnika, tehničko crtanje, električne instalacije\n\n• Maturalni rad: Izrada programske podrške za Hexapoda'
    },
    uni: {
      title: 'Prvostupnik informatike (bacc. inf.)',
      location: 'Odjel za informatiku, Sveučilište u Rijeci,\nRijeka (Hrvatska)',
      content: '• Osnove u C++, Python, C#, PHP i Java programskim jezicima, izrada Web aplikacija (HTML, CSS3), izrada multimedijskih elemenata (Adobe Photoshop, Illustrator, Premiere, Audition i Animate), samostalna izrada računalnih igara (Unity), modeliranje informacijskih sustava, baze podataka (SQL), osnove računalnog vida, vjerojatnost i statistika, rad u raznim programima za izradu softvera (Android Studio, CodeBlocks, Clarion, IntelliJ Idea, Netbeans, PyCharm, VS Code), rad na Linux operacijskom sustavu\n\n• Završni rad: Izrada Bookworm 2D edukativne igre u Unity alatu'
    },
    uni2: {
      title: 'Magistar informatike (mag. inf.)',
      location: 'Odjel za informatiku, Sveučilište u Rijeci,\nRijeka (Hrvatska)',
      content: '• Poslovna inteligencija i sustavi za podršku odlučivanja, data mining, ETL, distribuirani sustavi, inteligentni sustavi (strojno i dubinsko učenje, R jezik), e-obrazovanje, osnove digitalne obrade govora i slika, mrežni sustavi, Git (CI/CD), otkrivanje znanja u podacima\n\n• Diplomski rad: Izrada programa za ugostiteljstvo'
    }
  },
  workExperience: {
    dignitas1: {
      title: 'Voditelj radionice',
      location: 'Udruga Dignitas, Osnovna škola Zamet,\nRijeka (Hrvatska)',
      content: '• Voditelj radionice za izradu videoigara (Game Maker Studio) u trajanju od tjedan dana za osnovnoškolce u OŠ Zamet, Rijeka (10 polaznika) 2016. godine\n\n• Pseudojezik, osnove C programskog jezika i rasterska grafika',
    },
    frff: {
      title: 'Voditelj radionice',
      location: 'Udruga Dignitas, Four River Film Festival,\nKarlovac (Hrvatska)',
      content: '• Voditelj radionice za izradu videoigara (Game Maker Studio) u trajanju od tri dana za srednjoškolce u Gimnaziji, Karlovac (10 polaznika) 2017. godine\n\n• Pseudojezik, osnove C programskog jezika i rasterska grafika',
    },
    uni: {
      title: 'Studentski zbor Odjela za Informatiku',
      location: 'Odjel za informatiku, Sveučilište u Rijeci,\nRijeka (Hrvatska)',
      content: '• Predstavnik studenata 3. godine preddiplomskog studija - 2018. godina\n\n• Predstavnik studenata u Odboru za kvalitetu - 2018. do 2019. godina\n\n• Volontiranje na STEM Games-u u Poreču - 2018. godina\n\n• Glavni organizator LAN partya na Odjelu za informatiku (80 natjecatelja) - Natjecanje iz CS: GO, Dota 2 i FIFA - OBS Studio i Twitch streaming - 2019. godina',
    },
    iw: {
      title: 'Volonter Full-stack developer',
      location: 'IstraWine,\nNa daljinu',
      content: '• Razvoj web aplikacija i programa aktualnim tehnologijama\n\n• Frontend: Quasar Framework, Vue.js, Typescript (VS Code)\n\n• Backend: Drogon Framework C++\n\n• Database: Postgresql (DBeaver)\n\n• Dodatne tehnologije i alati: Python, Git, Git Extensions, Gitlab, CI/CD, Markdown, PuTTY, Figma, Inkscape, Photoshop, FontForge, Trello, Discord, Linux',
    },
    crc: {
      title: 'Software developer',
      location: 'CRC.ag,\nNa daljinu',
      content: '• Razvoj web aplikacija i programa aktualnim tehnologijama\n\n• Frontend: Angular (Angular Material), Vue, Typescript (VS Code)\n\n• Backend: Spring Boot, Kotlin (IntellJ), Java\n\n• Database: Oracle, MySQL (DBeaver), MongoDB\n\n• Dodatne tehnologije i alati: Git, Git Extensions, Gitlab, BitBucket, Markdown, PuTTY, Figma, Inkscape, Photoshop, Atlasian JIRA, CI/CD, Jenkins, Slack',
    },
    benazic: {
      title: 'Mentor studentu na stručnoj praksi',
      location: 'IstraWine, Fakultet informatike i digitalnih tehnologija\nNa daljinu',
      content: '• Mentor na stručnoj praksi za studenta s Fakulteta informatike i digitalnih tehnologija\n\n• Dizajn i razvoj web aplikacije za vinariju korištenjem aktualnih tehnologija\n\n• Frontend: Quasar Framework, Vue.js, Typescript (VS Code)\n\n• Backend: Drogon Framework C++\n\n• Database: Postgresql (DBeaver)\n\n• Dodatne tehnologije i alati: Python, Git, Git Extensions, Gitlab, CI/CD, Markdown, PuTTY, Figma, Inkscape, Photoshop, FontForge, Trello, Discord, Linux'
    }
  },
  projects: {
    nonaMare: {
      title: 'Nona Mare',
      description: 'Web stranica za firmu Nona Mare iz Rijeke koja sadrži razne proizvode iz domaće proizvodnje poput čajeva, začina i bilja. Također sadrži integrirani IstraWine webshop.',
    },
    benazićVina: {
      title: 'Benazić Vina',
      description: 'Web stranica izrađena od strane studenta pod mojim mentorstvom. Izrađena je stranica vinarije s integriranim IstraWine webshopom na kojoj možete kupiti vinske proizvode',
    },
    jasminMakajCV: {
      title: 'Jasmin Makaj CV',
      description: 'Web stranica koja sadrži sve informacije o mom obrazovanju, radnom iskustvu i većini projekata na kojima sam radio.',
    },
    nakshop: {
      title: 'Nakshop',
      description: 'Web stranica s integriranim IstraWine webshopom koja prodaje sigurne, ekonomske i ekološke ambalaže za bilo koju vrstu boce.',
    },
    luxuryLineAutotepisi: {
      title: 'Luxury Line Autotepisi',
      description: 'Luxury Line Autotepisi je web stranica koja prodaje premium i luksuzne auto tepihe.',
    },
    konobaBokun: {
      title: 'Konoba Bokun',
      description: 'Web stranica restorana Konobe Bokun s integriranim IstraWine webshopom na kojem možete kupiti domaće maslinovo ulje proizvodeno od obitelji Prodan.',
    },
    dDInnovation: {
      title: 'DD Innovation',
      description: 'DD Innovation web stranica s integriranim IstraWine webshopom na kojem možete pronaći sve potrebne alate za lončare, graditelje, kućne slikare, električare, vodoinstalatere, stolarije, montažere i zidare.',
    },
    sosichWines: {
      title: 'Sosich Wines',
      description: 'Sosich Wines web stranica s integriranim IstraWine webshopom na kojem se nalaze njihovi proizvodi, uključujući vina, destilati i maslinova ulja.',
    },
    istraWine: {
      title: 'Istra Wine',
      description: 'Istra Wine je platforma koja digitalizira vinarije, destilerije i OPG-ove sa šireg istarskog područja, čineći njihove proizvode dostupnim svima u samo nekoliko klikova.',
    },
    victorySoftware: {
      title: 'Victory software',
      description: 'Victory software & e-business je mala obiteljska tvrtka specijalizirana za razvoj softvera, e-commerce i savjetovanje.',
    },
    exploreCres: {
      title: 'Explore Cres',
      description: 'Predložak web stranice koji sadrži informacije o otoku Cresu, uključujući atrakcije, smještajne objekte i savjete za putovanje.',
    },
    bookworm: {
      title: 'Bookworm',
      description: 'Bookworm je 2D edukativna igra za djecu na hrvatskom jeziku.',
    },
    swampAttack: {
      title: 'SwampAttack',
      description: 'SwampAttack je mala 2D igra obrane kule u kojoj morate pucati na zombije.',
    },
    skullKnight: {
      title: 'SkullKnight',
      description: 'SkullKnight je mala 3D igra napravljena kao projekt na fakultetu.',
    },
    programForHospitalityFiskalVictory: {
      title: 'Program za ugostiteljstvo - FiskalVictory',
      description: 'Diplomski rad - Automatsko ažuriranje za korisnike programa, upravljanje licencama i registracijskim sustavima, slanje pojedinosti o novim korisnicima putem e-pošte vlasniku programa i izrada računa, korisnika, proizvoda, grupa, kategorija i računa u PDF formatu te još mnogo toga.',
    },
    n1NewsPortalArticleAnalyzer: {
      title: 'Analizator članaka N1 vijesti',
      description: 'Skupljanje svih članaka s N1 vijesti, analiza riječi, vizualizacija i interpretacija rezultata. Istraživanje članaka o COVID-19.',
    },
    emailSenderToApartmentOwners: {
      title: 'Slanje e-pošte vlasnicima apartmana',
      description: 'Alat za pretraživanje adresa e-pošte na web stranici turističkih informacija i automatsko slanje e-pošte s dinamičkom predloškom.',
    },
    effectsOfCOVID19VirusOnNumberOfPlayersOnSteamPlatform: {
      title: 'Utjecaj virusa COVID-19 na broj igrača na Steam platformi',
      description: 'Analiza Steam platforme - što se dogodilo s brojem igrača tijekom zaključavanja zbog COVID-19.',
    },
    thumbnailGeneratorForListOfProducts: {
      title: 'Generator minijatura za popis proizvoda',
      description: 'Alat koji pokušava mapirati minijature na proizvode koristeći algoritam Levensteinove udaljenosti.',
    },
    songScrapingToolAndAutomaticDownloader: {
      title: 'Alat za pretraživanje pjesama i automatsko preuzimanje',
      description: 'Alat koji automatski odlazi na radio svaku minutu i skuplja podatke o trenutnoj pjesmi. Popis pjesama se sprema i kasnije se može koristiti za automatsko preuzimanje glazbe s YouTubea.',
    },
    hexapodProgram: {
      title: 'Program za Hexapod',
      description: 'Hexapod je robot sa 6 nogu. Programski jezik Arduino, Arduino ploča, 18 elektromotora.',
    },
    scriptGeneratorForCounterStrikeGlobalOffensive: {
      title: 'Generator skripti za\nCounter-Strike: Global Offensive',
      description: 'Ovaj alat je mali generator skripti za kupovinu u igri Counter-Strike: Global Offensive. Korisnik može odabrati različito oružje i vezati ga za određenu tipku. Igrač kasnije može koristiti tu skriptu za automatsku kupovinu svega potrebnog jednim klikom.'

    },
    scriptGeneratorForCounterStrikeSource: {
      title: 'Generator skripti za\nCounter-Strike: Source',
      description: 'Ovaj alat je mali generator skripti za kupovinu u igri Counter-Strike: Source. Korisnik može odabrati različito oružje i vezati ga za određenu tipku. Igrač kasnije može koristiti tu skriptu za automatsku kupovinu svega potrebnog jednim klikom.'
    }
  }
};
