export default {
  untilToday: 'Until Today',
  year: 'Year',
  years: 'Years',
  month: 'Month',
  months: 'Months',
  website: 'Websites',
  webdesign: 'Web design',
  games: 'Games',
  misc: 'Miscellaneous',
  intro: {
    title: 'Introduction',
    text: "I have worked with a wide range of technologies, however I'm always motivated to learn more and use the latest technology available. Along with having excellent communication skills, I also have a lot of experience working in small teams."
  },
  languages: {
    title: 'Languages',
    croatian: 'Croatian',
    german: 'German',
    english: 'English',
  },
  contact: {
    title: 'Contact'
  },
  nav: {
    introduction: 'Introduction',
    workExperience: 'Work Experience',
    projects: 'My Projects',
    education: 'My Education',
    selectLanguage: 'Select Language',
    openInGitlab: 'Show Gitlab Project',
    showVideo: 'Show Video',
    openWebsite: 'Show Project'
  },
  education: {
    highschool: {
      title: 'Mechatronics technician',
      location: 'Electrical engineering school,\nVaraždin (Croatia)',
      content: '• Developing software in C# programming language, microcontroller programming, LOGO devices (Logo! Soft) and PLC devices for industry production line automatization (Siemens SIMATIC), Arduino devices, robotics, robot arms, renewable energy sources, hydraulics, pneumatics, CNC machines, 3D modeling (Solid Works), mechanical engineering, quality assurance, electrical engineering, technical drawing, electrical installations\n\n• Final assignment: Program for Hexapod '
    },
    uni: {
      title: 'Bachelor of informatics (bacc. inf.)',
      location: 'Department of Informatics University of Rijeka,\nRijeka (Croatia)',
      content: '• The basics of C++, Python, C#, PHP, Java, creating basic Web applications (HTML, CSS3), using multimedia tools (Adobe Photoshop, Adobe Illustrator, Adobe Premiere, Adobe Audition and Adobe Animate), developing small computer games using Unity tool, business system modeling, databases (SQL), the basics of computer vision, probability and statistics, working with various tools (Android Studio, CodeBlocks, Clarion, IntelliJ Idea, Netbeans, PyCharm, VS Code), working on Linux operating systems\n\n• Final assignment: Bookworm 2D educative game in Unity'
    },
    uni2: {
      title: 'Master of informatics (mag. inf.)',
      location: 'Department of Informatics University of Rijeka,\nRijeka (Croatia)',
      content: '• Business intelligence and decision support systems, data mining, ETL, distributed systems, intelligent systems (Machine learning and deep learning, R programming language), e-education, the basics of digital speech and image processing, network systems, Git (CI/CD)\n\n• Final assignment: Program for hospitality (Fiscalization) '
    }
  },
  workExperience: {
    dignitas1: {
      title: 'Game development workshop',
      location: 'Dignitas association, Zamet Elementary School,\nRijeka (Croatia)',
      content: '• Teacher in Game development workshop 2016. (Game Maker Studio) for one week long workshop for elementary school students in Rijeka (10 participants)\n\n• Pseudolanguage, the basics of C programming language and raster graphics',
    },
    frff: {
      title: 'Game development workshop',
      location: 'Dignitas association, Four River Film Festival,\nKarlovac (Croatia)',
      content: '• Teacher in Game development workshop 2016. (Game Maker Studio) for three days for highschool students in Karlovac 2017. (10 participants)\n\n• Pseudolanguage, the basics of C programming language and raster graphics',
    },
    uni: {
      title: 'The councial of students in Department of Informatics',
      location: 'Department of Informatics University of Rijeka,\nRijeka (Croatia)',
      content: '• Student representative of the 3rd year pregraduate students - 2018.\n\n• Student representative in Quality Assurance Committee - 2018. and 2019.\n\n• Volunteering on STEM Games in Poreč - 2018.\n\n• Lead Organizer of LAN party on the Department of Informatics (80 participants) - Competition in CS: GO, Dota 2 and FIFA (OBS Studio and Twitch streaming) - 2019.',
    },
    iw: {
      title: 'Volunteer Full-stack developer',
      location: 'IstraWine,\nRemote',
      content: '• Development of web applications and software using latest technologies.\n\n• Frontend: Quasar Framework, Vue.js, Typescript (VS Code).\n\n• Backend: Drogon Framework C++.\n\n• Database: Postgresql (DBeaver).\n\n• Additional technologies and tools: Python, Git, Git Extensions, Gitlab, CI/CD, Markdown, PuTTY, Figma, Inkscape, Photoshop, FontForge, Trello, Discord, Linux.',
    },
    crc: {
      title: 'Software developer',
      location: 'CRC.ag,\nRemote',
      content: '• Development of web applications and software using latest technologies.\n\n• Frontend: Angular (Angular Material), Vue, Typescript (VS Code)\n\n• Backend: Spring Boot, Kotlin (IntellJ), Java\n\n• Database: Oracle, MySQL (DBeaver), MongoDB\n\n• Additional tools and technologies: Git, Git Extensions, Gitlab, BitBucket, Markdown, PuTTY, Figma, Inkscape, Photoshop, Atlasian JIRA, CI/CD, Jenkins, Slack',
    },
    benazic: {
      title: 'Mentor to a student',
      location: 'IstraWine, Faculty of Computer Science and Digital Technologies\nRemote',
      content: '• Mentoring a student from the Faculty of Computer Science and Digital Technologies on their professional practice\n\n• Designing and developing a web application for a winery using current technologies\n\n• Frontend: Quasar Framework, Vue.js, Typescript (VS Code)\n\n• Backend: Drogon Framework C++\n\n• Database: Postgresql (DBeaver)\n\n• Additional technologies and tools: Python, Git, Git Extensions, Gitlab, CI/CD, Markdown, PuTTY, Figma, Inkscape, Photoshop, FontForge, Trello, Discord, Linux'
    }
  },
  projects: {
    nonaMare: {
      title: 'Nona Mare',
      description: 'Nona Mare Website is a domestic production of various teas, spices, and herbs from Rijeka. It also contains integraded IstraWine webshop.',
    },
    benazićVina: {
      title: 'Benazić Vina',
      description: 'Winery website with integrated IstraWine webshop that sells mostly wine products created by a student under my mentorship.',
    },
    jasminMakajCV: {
      title: 'Jasmin Makaj CV',
      description: 'Website that contains all the information about my education, work experience and most of the projects that I\'ve worked on.',
    },
    nakshop: {
      title: 'Nakshop',
      description: 'Website with integrated IstraWine webshop that sells safe, economical and ecological packagings for any type of bottle.',
    },
    luxuryLineAutotepisi: {
      title: 'Luxury Line Autotepisi',
      description: 'Luxury Line Autotepisi is a website that sells premium and luxurious car mats.',
    },
    konobaBokun: {
      title: 'Konoba Bokun',
      description: 'Tavern Bokun restaurant\'s website with integrated IstraWine webshop that sells homemade olive oil made by Family Prodan.',
    },
    dDInnovation: {
      title: 'DD Innovation',
      description: 'DD Innovation website with integrated IstraWine webshop that sells all necessary tools for potters, builders, house painters, electricians, plumbers, carpenters, fitters and masons.',
    },
    sosichWines: {
      title: 'Sosich Wines',
      description: 'Sosich Wines website with integrated IstraWine webshop that sells their products, including wines, destillates and olive oils',
    },
    istraWine: {
      title: 'Istra Wine',
      description: 'Istra Wine is a platform that digitizes the wine of Istria, making it available for you from the comfort of your home only by few clicks.',
    },
    victorySoftware: {
      title: 'Victory software',
      description: 'Victory software & e-business is a small familly company that specializes in software development',
    },
    exploreCres: {
      title: 'Explore Cres',
      description: 'Website template that contains informations about the island of Cres, including its attractions, accommodations, and travel tips.',
    },
    bookworm: {
      title: 'Bookworm',
      description: 'Bookworm is a 2D educational game for kids in Croatian language.',
    },
    swampAttack: {
      title: 'SwampAttack',
      description: 'SwampAttack is a small 2D tower defence game where you have to shoot zombies.',
    },
    skullKnight: {
      title: 'SkullKnight',
      description: 'SkullKnight is small 3D game made as a class project.',
    },
    programForHospitalityFiskalVictory: {
      title: 'Program for hospitality - FiskalVictory',
      description: 'Final assignment - Automatic update for program users, licence managment and regestration systems, sending details about new users via email to program owner and receipt, user, product, group, category, receipts to PDF and more'
    },
    n1NewsPortalArticleAnalyzer: {
      title: 'N1 news portal article analyzer',
      description: 'Scraping all articles from N1 news portal, analyzing words and visualizing and interpreting results. Research about COVID-19 articles',
    },
    emailSenderToApartmentOwners: {
      title: 'Email sender to apartment owners',
      description: 'Tool that scraps for email addresses on tourist information website and automatically sends email with dynamic template.',
    },
    effectsOfCOVID19VirusOnNumberOfPlayersOnSteamPlatform: {
      title: 'Effects of COVID-19 virus on number of players on Steam platform',
      description: 'Steam platform analysis - what happened to number of players during COVID-19 lockdown',
    },
    thumbnailGeneratorForListOfProducts: {
      title: 'Thumbnail generator for list of products ',
      description: 'Tool that tries to map thumbnails to product using Levenstein distance algorithm.',
    },
    songScrapingToolAndAutomaticDownloader: {
      title: 'Song scraping tool and automatic downloader',
      description: 'Tool that automatically goes to radio every minute and scraps song that is currently playing. Song list is saved and can later be used to automatically download music from youtube.',
    },
    hexapodProgram: {
      title: 'Hexapod program',
      description: 'Hexapod is a robot with 6 legs. Arduino programming language, Arduino board, 18 electric motors.',
    },
    scriptGeneratorForCounterStrikeGlobalOffensive: {
      title: 'Script generator for\nCounter-Strike: Global Offensive',
      description: 'This tool is a small buy script generator for Counter-Strike: Global Offensive. User can select various weapons and bind it to a key. Player can later use this script to automatically buy everything needed withh just one click.'
    },
    scriptGeneratorForCounterStrikeSource: {
      title: 'Script generator for\nCounter-Strike: Source',
      description: 'This tool is a small buy script generator for Counter-Strike: Source. User can select various weapons and bind it to a key. Player can later use this script to automatically buy everything needed with just one click.'
    }
  }
};
