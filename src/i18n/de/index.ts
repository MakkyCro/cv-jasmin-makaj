export default {
  untilToday: 'Bis Heute',
  year: 'Jahr',
  years: 'Jahren',
  month: 'Monat',
  months: 'Monate',
  website: 'Webseiten',
  webdesign: 'Web-Design',
  games: 'Spiele',
  misc: 'Sonstig',
  intro: {
    title: 'Einführung',
    text: 'Ich habe mit einer Vielzahl von Technologien gearbeitet, bin jedoch immer motiviert, mehr zu lernen und die neueste verfügbare Technologie zu nutzen. Neben ausgezeichneten Kommunikationsfähigkeiten verfüge ich auch über viel Erfahrung in der Arbeit in kleinen Teams.'
  },
  languages: {
    title: 'Sprachen',
    croatian: 'Kroatisch',
    german: 'Deutsch',
    english: 'Englisch',
  },
  contact: {
    title: 'Kontakt'
  },
  nav: {
    introduction: 'Einführung',
    workExperience: 'Berufserfahrung',
    projects: 'Meine Projekte',
    education: 'Meine Ausbildung',
    selectLanguage: 'Sprache Auswählen',
    openInGitlab: 'Gitlab Projekt anzeigen',
    showVideo: 'Video zeigen',
    openWebsite: 'Projekt zeigen'
  },
  education: {
    highschool: {
      title: 'Mechatroniker-Techniker',
      location: 'Elektrotechnikschule, Varaždin (Kroatien)',
      content: '• Entwicklung von Software in der Programmiersprache C#, Mikrocontroller-Programmierung, LOGO-Geräte (Logo! Soft) und PLC-Geräte für die Automatisierung von Industrieproduktionslinien (Siemens SIMATIC), Arduino-Geräte, Robotik, Roboterarme, erneuerbare Energiequellen, Hydraulik, Pneumatik, CNC-Maschinen, 3D-Modellierung (Solid Works), Maschinenbau, Qualitätssicherung, Elektrotechnik, technisches Zeichnen, elektrische Installationen.\n\n• Abschlussarbeit: Hexapod-Programm.'
    },
    uni: {
      title: 'Bachelor of Informatik (bacc. inf.)',
      location: 'Abteilung für Informatik,\nUniversität Rijeka (Kroatien)',
      content: '• Grundlagen von C++, Python, C#, PHP, Java, Erstellung von grundlegenden Webanwendungen (HTML, CSS3), Verwendung von Multimedia-Tools (Adobe Photoshop, Adobe Illustrator, Adobe Premiere, Adobe Audition und Adobe Animate), Entwicklung von kleinen Computerspielen mit Unity-Tool, Modellierung von Geschäftssystemen, Datenbanken (SQL), Grundlagen der Computer Vision, Wahrscheinlichkeitsrechnung und Statistik, Arbeit mit verschiedenen Tools (Android Studio, CodeBlocks, Clarion, IntelliJ Idea, Netbeans, PyCharm, VS Code), Arbeit mit Linux-Betriebssystemen.\n\n• Abschlussarbeit: Bookworm 2D-Lernspiel in Unity.'
    },
    uni2: {
      title: 'Master of Informatik (mag. inf.)',
      location: 'Abteilung für Informatik,\nUniversität Rijeka (Kroatien)',
      content: '• Business Intelligence und Entscheidungsunterstützungssysteme, Data Mining, ETL, verteilte Systeme, intelligente Systeme (Machine Learning und Deep Learning, R-Programmiersprache), E-Learning, Grundlagen der digitalen Sprach- und Bildverarbeitung, Netzwerksysteme, Git (CI/CD).\n\n• Abschlussarbeit: Programm für die Gastfreundschaft (Fiskalisierung).'
    }
  },
  workExperience: {
    dignitas1: {
      title: 'Workshop für Spieleentwicklung',
      location: 'Dignitas Verein, Grundschule Zamet,\nRijeka (Kroatien)',
      content: '• Lehrer im Workshop für Spieleentwicklung 2016 (Game Maker Studio) für eine einwöchige Workshop für Grundschüler in Rijeka (10 Teilnehmer)\n\n• Pseudosprache, Grundlagen der Programmiersprache C und Rastergrafiken',
    },
    frff: {
      title: 'Workshop für Spieleentwicklung',
      location: 'Dignitas Verein, Four River Film Festival,\nKarlovac (Kroatien)',
      content: '• Lehrer im Workshop für Spieleentwicklung 2016 (Game Maker Studio) für drei Tage für Schüler in Karlovac 2017. (10 Teilnehmer)\n\n• Pseudosprache, Grundlagen der Programmiersprache C und Rastergrafiken',
    },
    uni: {
      title: 'Studentenrat im Fachbereich Informatik',
      location: 'Fachbereich Informatik, Universität Rijeka,\nRijeka (Kroatien)',
      content: '• Studentenvertreter - im 3. Studienjahr - 2018.\n\n• Studentenvertreter im Ausschuss für Qualitätssicherung - 2018 und 2019.\n\n• Freiwilliger auf den STEM Games in Poreč - 2018.\n\n• Hauptorganisator des LAN-Partys am Fachbereich Informatik (80 Teilnehmer) - Wettkampf in CS:GO, Dota 2 und FIFA (OBS Studio und Twitch-Streaming) - 2019.',
    },
    iw: {
      title: 'Volontär Full-Stack Entwickler',
      location: 'IstraWine,\nRemote',
      content: '• Entwicklung von Webanwendungen und Software mit den neuesten Technologien.\n\n• Frontend: Quasar Framework, Vue.js, Typescript (VS Code).\n\n• Backend: Drogon Framework C++.\n\n• Datenbank: Postgresql (DBeaver).\n\n• Zusätzliche Technologien und Tools: Python, Git, Git Extensions, Gitlab, CI/CD, Markdown, PuTTY, Figma, Inkscape, Photoshop, FontForge, Trello, Discord, Linux.',
    },
    crc: {
      title: 'Softwareentwickler',
      location: 'CRC.ag,\nRemote',
      content: '• Entwicklung von Webanwendungen und Software mit den neuesten Technologien.\n\n• Frontend: Angular (Angular Material), Vue, Typescript (VS Code)\n\n• Backend: Spring Boot, Kotlin (IntellJ), Java\n\n• Datenbank: Oracle, MySQL (DBeaver), MongoDB\n\n• Zusätzliche Technologien und Tools: Git, Git Extensions, Gitlab, BitBucket, Markdown, PuTTY, Figma, Inkscape, Photoshop, Atlasian JIRA, CI/CD, Jenkins, Slack',
    },
    benazic: {
      title: 'Mentor für einen Studenten',
      location: 'IstraWine, Fakultät für Informatik und digitale Technologien\nRemote',
      content: '• Mentoring eines Studenten der Fakultät für Informatik und digitale Technologien während seiner Berufspraxis\n\n• Entwurf und Entwicklung einer Webanwendung für eine Weinerei mit aktuellen Technologien\n\n• Frontend: Quasar Framework, Vue.js, Typescript (VS Code)\n\n• Backend: Drogon Framework C++\n\n• Datenbank: Postgresql (DBeaver)\n\n• Weitere Technologien und Tools: Python, Git, Git Extensions, Gitlab, CI/CD, Markdown, PuTTY, Figma, Inkscape, Photoshop, FontForge, Trello, Discord, Linux'
    }
  },
  projects: {
    nonaMare: {
      title: 'Nona Mare',
      description: 'Die Nona Mare-Website ist eine inländische Produktion verschiedener Tees, Gewürze und Kräuter aus Rijeka. Sie enthält auch den integrierten IstraWine-Webshop.',
    },
    benazićVina: {
      title: 'Benazić Vina',
      description: 'Weinwebsite mit integriertem IstraWine-Webshop, der hauptsächlich von einem Studenten unter meiner Anleitung erstellte Weinprodukte verkauft.',
    },
    jasminMakajCV: {
      title: 'Jasmin Makaj CV',
      description: 'Website, die alle Informationen über meine Ausbildung, Berufserfahrung und die meisten Projekte enthält, an denen ich gearbeitet habe.',
    },
    nakshop: {
      title: 'Nakshop',
      description: 'Website mit integriertem IstraWine-Webshop, der sichere, wirtschaftliche und ökologische Verpackungen für jede Art von Flasche verkauft.',
    },
    luxuryLineAutotepisi: {
      title: 'Luxury Line Autotepisi',
      description: 'Luxury Line Autotepisi ist eine Website, die Premium- und luxuriöse Autoteppiche verkauft.',
    },
    konobaBokun: {
      title: 'Konoba Bokun',
      description: 'Die Website des Restaurants Tavern Bokun mit integriertem IstraWine-Webshop, der hausgemachtes Olivenöl der Familie Prodan verkauft.',
    },
    dDInnovation: {
      title: 'DD Innovation',
      description: 'DD Innovation-Website mit integriertem IstraWine-Webshop, der alle erforderlichen Werkzeuge für Töpfer, Bauarbeiter, Hausmaler, Elektriker, Klempner, Schreiner, Installateure und Maurer verkauft.',
    },
    sosichWines: {
      title: 'Sosich Wines',
      description: 'Sosich Wines-Website mit integriertem IstraWine-Webshop, der ihre Produkte, einschließlich Weine, Destillate und Olivenöle, verkauft.',
    },
    istraWine: {
      title: 'Istra Wine',
      description: 'Istra Wine ist eine Plattform, die den Wein Istriens digitalisiert und ihn Ihnen mit nur wenigen Klicks bequem von zu Hause aus zur Verfügung stellt.',
    },
    victorySoftware: {
      title: 'Victory Software',
      description: 'Victory Software & E-Business ist ein kleines Familienunternehmen, das sich auf Softwareentwicklung spezialisiert hat.',
    },
    exploreCres: {
      title: 'Explore Cres',
      description: 'Website-Vorlage, die Informationen über die Insel Cres enthält, einschließlich ihrer Sehenswürdigkeiten, Unterkünfte und Reisetipps.',
    },
    bookworm: {
      title: 'Bookworm',
      description: 'Bookworm ist ein zweidimensionales Lernspiel für Kinder in kroatischer Sprache.',
    },
    swampAttack: {
      title: 'SwampAttack',
      description: 'SwampAttack ist ein kleines zweidimensionales Tower-Defense-Spiel, bei dem man Zombies erschießen muss.',
    },
    skullKnight: {
      title: 'SkullKnight',
      description: 'SkullKnight ist ein kleines 3D-Spiel, das im Rahmen eines Klassenprojekts erstellt wurde.',
    },
    programForHospitalityFiskalVictory: {
      title: 'Programm für die Gastronomie - FiskalVictory',
      description: 'Abschlussarbeit - Automatisches Update für Programmnutzer, Lizenzverwaltung und Registriersysteme, Versenden von Details über neue Benutzer per E-Mail an den Programmbesitzer sowie Erstellung von Belegen, Benutzern, Produkten, Gruppen, Kategorien und Belegübersichten im PDF-Format und vieles mehr.',
    },
    n1NewsPortalArticleAnalyzer: {
      title: 'N1 Newsportal-Artikelanalysator',
      description: 'Auslesen aller Artikel vom N1 Newsportal, Analyse der Wörter und Visualisierung und Interpretation der Ergebnisse. Forschung zu COVID-19-Artikeln.',
    },
    emailSenderToApartmentOwners: {
      title: 'E-Mail-Sender an Apartmentbesitzer',
      description: 'Tool zum Auslesen von E-Mail-Adressen auf einer Tourismusinformations-Website und automatischen Versenden von E-Mails mit dynamischen Vorlagen.',
    },
    effectsOfCOVID19VirusOnNumberOfPlayersOnSteamPlatform: {
      title: 'Auswirkungen des COVID-19-Virus auf die Anzahl der Spieler auf der Steam-Plattform',
      description: 'Analyse der Steam-Plattform - Was ist während der COVID-19-Ausgangsbeschränkungen mit der Anzahl der Spieler passiert?',
    },
    thumbnailGeneratorForListOfProducts: {
      title: 'Thumbnail-Generator für Produktlisten',
      description: 'Tool, das versucht, Miniaturansichten anhand des Levenstein-Distanzalgorithmus den Produkten zuzuordnen.',
    },
    songScrapingToolAndAutomaticDownloader: {
      title: 'Lied-Auslesewerkzeug und automatischer Downloader',
      description: 'Tool, das automatisch auf einen Radiosender wechselt und das gerade gespielte Lied ausliest. Die Songliste wird gespeichert und kann später verwendet werden, um Musik von YouTube automatisch herunterzuladen.',
    },
    hexapodProgram: {
      title: 'Hexapod-Programm',
      description: 'Ein Hexapod ist ein Roboter mit 6 Beinen. Programmiersprache Arduino, Arduino-Board, 18 Elektromotoren.',
    },
    scriptGeneratorForCounterStrikeGlobalOffensive: {
      title: 'Skriptgenerator für Counter-Strike: Global Offensive',
      description: 'Dieses Tool ist ein kleiner Buy-Skript-Generator für Counter-Strike: Global Offensive. Der Benutzer kann verschiedene Waffen auswählen und sie einer Taste zuweisen. Der Spieler kann später dieses Skript verwenden, um mit nur einem Klick automatisch alles Notwendige zu kaufen.',
    },
    scriptGeneratorForCounterStrikeSource: {
      title: 'Skriptgenerator für Counter-Strike: Source',
      description: 'Dieses Tool ist ein kleiner Buy-Skript-Generator für Counter-Strike: Source. Der Benutzer kann verschiedene Waffen auswählen und sie einer Taste zuweisen. Der Spieler kann später dieses Skript verwenden, um mit nur einem Klick automatisch alles Notwendige zu kaufen.',
    }
  }
};
