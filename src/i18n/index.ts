import en from './en';
import hr from './hr';
import de from './de';

export default {
  'en': en,
  'hr': hr,
  'de': de,
};
