import { ApexOptions } from 'apexcharts';

export const radialBarStyle: ApexOptions = {
    chart: {
        animations: {
            enabled: true,
            dynamicAnimation: {
                speed: 800
            }
        },
        background: 'transparent',
        foreColor: '#FFFFFF',
        height: 100,
        toolbar: {
            show: false
        },
        type: 'radialBar',
        width: 100
    },
    plotOptions: {

        radialBar: {
            hollow: {
                background: 'transparent',
                dropShadow: {}
            },
            track: {
                startAngle: 0,
                endAngle: 0,
                background: '#606082',
                strokeWidth: '100%',
                margin: 1
            },
            dataLabels: {
                name: {
                    show: false,
                },
                value: {
                    show: false,
                }
            }
        },
    },
    colors: [
        '#FFC742',
        '#c7f464',
        '#81D4FA',
        '#fd6a6a',
        '#546E7A'
    ],
    dataLabels: {
        style: {
            fontWeight: 600,
        }
    },
    grid: {
        borderColor: 'transparent',
        padding: {
            right: 0,
            bottom: 0,
            left: 0
        }
    },
    stroke: {
        lineCap: 'round'
    },
    tooltip: {
        enabled: false,
        fillSeriesColor: true
    },
}