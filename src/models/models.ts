export interface Language {
  languageName: string;
  level: string;
  percentage: number;
}

export interface ContactItem {
  icon: string;
  label: string;
  value: string;
  href?: string;
}

export interface CardInfo {
  title: string;
  text?: string;
  to?: ToLink;
}

export interface ToLink {
  name: string;
}

export interface NavItem {
  id?: number;
  title: string;
  routerLink?: string;
  click?: (emit: (event: string, ...args: unknown[]) => void, self: NavItem) => void;
  value?: unknown;
  icon?: string;
  class?: string;
  disabled?: boolean;
  children?: Array<NavItem>;
  separatorBefore?: boolean;
  onlyDrawer?: boolean;
}

export interface TimelinePeriod {
  title: string;
  events: TimelineEvent[];
}

export interface TimelineEvent {
  title: string;
  location?: string;
  locationUrl?: string;
  date?: Date;
  dateUntil?: Date | string;
  content?: string;
  buttons?: NavItem[];
}

export interface ProjectList {
  projectType: ProjectTypes;
  projects: Project[];
}

export interface Project {
  title: string;
  description: string;
  date: Date;
  imageSrc?: string;
  imageSrcSet?: string;
  liveUrl?: string;
  projectUrl?: string;
  videoUrl?: string;
  technologies?: TechnologyTypes[];
}

export enum ProjectTypes {
  WEBSITE = 'website',
  WEBDESIGN = 'webdesign',
  GAMES = 'games',
  MISC = 'misc'
}

export enum TechnologyTypes {
  // FRAMEWORKS
  QUASAR = 'quasar',
  VUEJS = 'vuejs',
  ANGULAR = 'angular',
  SPRINGBOOT = 'springboot',
  DROGON = 'drogon',
  DOTNET = 'dotnet',
  DOTNETCORE = 'dotnetcore',
  // LANGUAGES
  KOTLIN = 'kotlin',
  JAVASCRIPT = 'javascript',
  TYPESCRIPT = 'typescript',
  PYTHON = 'python',
  CPLUSPLUS = 'cplusplus',
  CSHARP = 'csharp',
  C = 'c',
  JAVA = 'java',
  SQL = 'sql',
  PHP = 'php',
  ARDUINO = 'arduino',
  MATLAB = 'matlab',
  R = 'r',
  // TOOLS
  VSCODE = 'vscode',
  VISUALSTUDIO = 'visualstudio',
  INTELLIJ = 'intellij',
  UNITY = 'unity',
  INKSCAPE = 'inkscape',
  FIGMA = 'figma',
  KRITA = 'krita',
  PHOTOSHOP = 'photoshop',
  ILLUSTRATOR = 'illustrator',
  AUDITION = 'audition',
  PREMIERE = 'premiere',
  ANIMATE = 'animate',
  AFTEREFFECTS = 'aftereffects',
  CLARION = 'clarion',
  MICROSTRATEGY = 'microstrategy',
  DBEAVER = 'dbeaver',
  FONTFORGE = 'fontforge',
  SONYVEGASPRO = 'sonyvegaspro',
  SOLIDWORKS = 'solidworks',
  SIMENS = 'simens',
  FILEZILLA = 'filezilla',
  WINSCP = 'winscp',
  OBSSTUDIO = 'obsstudio',
  GEPHI = 'gephi',
  NETBEANS = 'netbeans',
  XAMP = 'xamp',
  CODEBLOCKS = 'codeblocks',
  ANDROIDSTUDIO = 'androidstudio',
  BLENDER = 'blender',
  // OS
  LINUX = 'linux',
  WINDOWS = 'windows',
  // MISC
  GITLAB = 'gitlab',
  MARKDOWN = 'markdown',
  JINJA = 'jinja',
  BOOTSTRAP = 'bootstrap',
  HTML = 'HTML',
  TAILWINDCSS = 'tailwindcss',
  APACHE = 'apache',
  NGINX = 'nginx',
  AWS = 'aws',
  BEAUTIFULSOUP = 'beautifulsoup',
  SELENIUM = 'selenium',
  SCRAPY = 'scrapy',
  SCSS = 'scss',
  CICD = 'cicd',
  YOUTUBE = 'youtube',
  MIRO = 'miro',
  DIGITALOCEAN = 'digitalocean',
  TALEND = 'talend',
  TABLEAU = 'tableau',
  MARIADB = 'mariadb',
  SQLITE = 'sqlite',
  MKDOCS = 'mkdocs',
  PUTTY = 'putty',
  GITLABRUNNER = 'gitlabrunner'
}

export const techLogos = {
  quasar: { label: 'Quasar', hasImage: true },
  vuejs: { label: 'Vue.js', hasImage: true },
  angular: { label: 'Angular', hasImage: true },
  springboot: { label: 'Spring Boot', hasImage: true },
  drogon: { label: 'Drogon', hasImage: true },
  dotnet: { label: '.Net', hasImage: false },
  dotnetcore: { label: '.Net Core', hasImage: false },
  kotlin: { label: 'Kotlin', hasImage: false },
  javascript: { label: 'Javascript', hasImage: true },
  typescript: { label: 'Typescript', hasImage: true },
  python: { label: 'Python', hasImage: true },
  cplusplus: { label: 'C++', hasImage: true },
  csharp: { label: 'C#', hasImage: true },
  c: { label: 'C', hasImage: false },
  java: { label: 'Java', hasImage: false },
  sql: { label: 'Sql', hasImage: false },
  php: { label: 'Php', hasImage: false },
  arduino: { label: 'Arduino', hasImage: false },
  matlab: { label: 'Matlab', hasImage: false },
  r: { label: 'R', hasImage: false },
  vscode: { label: 'VS Code', hasImage: true },
  visualstudio: { label: 'Visual Studio', hasImage: true },
  intellij: { label: 'Intellij', hasImage: false },
  unity: { label: 'Unity', hasImage: true },
  inkscape: { label: 'Inkscape', hasImage: false },
  figma: { label: 'Figma', hasImage: true },
  krita: { label: 'Krita', hasImage: false },
  photoshop: { label: 'Photoshop', hasImage: true },
  illustrator: { label: 'Illustrator', hasImage: false },
  audition: { label: 'Audition', hasImage: false },
  premiere: { label: 'Premiere', hasImage: false },
  animate: { label: 'Animate', hasImage: false },
  aftereffects: { label: 'After Effects', hasImage: false },
  clarion: { label: 'Clarion', hasImage: false },
  microstrategy: { label: 'Microstrategy', hasImage: false },
  dbeaver: { label: 'DBeaver', hasImage: false },
  fontforge: { label: 'Font Forge', hasImage: false },
  sonyvegaspro: { label: 'Sony Vegas Pro', hasImage: false },
  solidworks: { label: 'Solid Works', hasImage: false },
  simens: { label: 'Simens', hasImage: false },
  filezilla: { label: 'Filezilla', hasImage: false },
  winscp: { label: 'WinSCP', hasImage: false },
  obsstudio: { label: 'OBS Studio', hasImage: false },
  gephi: { label: 'Gephi', hasImage: false },
  netbeans: { label: 'Netbeans', hasImage: false },
  xamp: { label: 'Xamp', hasImage: false },
  codeblocks: { label: 'Code Blocks', hasImage: false },
  androidstudio: { label: 'Android Studio', hasImage: false },
  blender: { label: 'Blender', hasImage: false },
  linux: { label: 'Linux', hasImage: false },
  windows: { label: 'Windows', hasImage: false },
  gitlab: { label: 'Gitlab', hasImage: true },
  markdown: { label: 'Markdown', hasImage: false },
  jinja: { label: 'Jinja', hasImage: false },
  bootstrap: { label: 'Bootstrap', hasImage: false },
  HTML: { label: 'HTML', hasImage: false },
  tailwindcss: { label: 'Tailwindcss', hasImage: true },
  apache: { label: 'Apache', hasImage: false },
  nginx: { label: 'Nginx', hasImage: false },
  aws: { label: 'AWS', hasImage: false },
  beautifulsoup: { label: 'Beautifulsoup', hasImage: false },
  selenium: { label: 'Selenium', hasImage: false },
  scrapy: { label: 'Scrapy', hasImage: false },
  scss: { label: 'SCSS', hasImage: false },
  cicd: { label: 'CI/CD', hasImage: false },
  youtube: { label: 'Youtube', hasImage: false },
  miro: { label: 'Miro', hasImage: false },
  digitalocean: { label: 'Digital Ocean', hasImage: false },
  talend: { label: 'Talend', hasImage: false },
  tableau: { label: 'Tableau', hasImage: false },
  mariadb: { label: 'MariaDB', hasImage: false },
  sqlite: { label: 'SQLite', hasImage: false },
  mkdocs: { label: 'Mkdocs', hasImage: false },
  putty: { label: 'PuTTy', hasImage: false },
  gitlabrunner: { label: 'Gitlab Runner', hasImage: false },
}
