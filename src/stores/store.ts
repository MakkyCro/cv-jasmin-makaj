import { defineStore } from 'pinia';
import { LocalStorage } from 'quasar';

export const useLocaleStore = defineStore('locale', {

  state: () => ({
    locale: LocalStorage.getItem('locale') || 'en',
  }),

  getters: {
    getLocale: (state) => () => {
      return state.locale;
    }
  },

  actions: {
    setLocale: (locale: string) => {
      LocalStorage.set('locale', locale)
    }
  }
});
