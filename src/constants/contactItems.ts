import { ContactItem } from 'src/models/models';

export const contactItems: ContactItem[] = [
    {
        icon: 'email',
        label: 'contact.labels.email',
        value: 'jasmin.makaj@gmail.com',
        href: 'mailto:jasmin.makaj@gmail.com'
    },
    {
        icon: 'fab fa-linkedin',
        label: 'contact.labels.linkedin',
        value: 'in/jasmin-makaj',
        href: 'https://www.linkedin.com/in/jasmin-makaj/'
    },
]