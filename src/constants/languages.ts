import { Language } from 'src/models/models';

export const languageList: Language[] = [
    {
        languageName: 'languages.croatian',
        level: 'Native',
        percentage: 100
    },
    {
        languageName: 'languages.english',
        level: 'C1/B2',
        percentage: 75
    },
    {
        languageName: 'languages.german',
        level: 'C1/B2',
        percentage: 75
    },
]