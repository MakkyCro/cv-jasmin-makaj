import { TimelinePeriod } from 'src/models/models';

export const educationList: TimelinePeriod[] = [
    {
        title: 'nav.education',
        events: [
            {
                date: new Date(2021, 8, 27),
                title: 'education.uni2.title',
                location: 'education.uni2.location',
                locationUrl: 'https://www.inf.uniri.hr',
                content: 'education.uni2.content',
                buttons: [{
                    title: 'nav.openInGitlab',
                    routerLink: 'https://gitlab.com/MakkyCro/fiskalvictory',
                    icon: 'fab fa-gitlab'
                }]
            },
            {
                date: new Date(2019, 8, 20),
                title: 'education.uni.title',
                location: 'education.uni.location',
                locationUrl: 'https://www.inf.uniri.hr',
                content: 'education.uni.content',
                buttons: [{
                    title: 'nav.openInGitlab',
                    routerLink: 'https://gitlab.com/MakkyCro/bookworm',
                    icon: 'fab fa-gitlab'
                }]
            },
            {
                date: new Date(2015, 4, 22),
                title: 'education.highschool.title',
                location: 'education.highschool.location',
                locationUrl: 'https://ess.hr',
                content: 'education.highschool.content',
                buttons: [{
                    title: 'nav.showVideo',
                    routerLink: 'https://www.youtube.com/embed/q2thpDE45zY',
                    icon: 'fab fa-youtube'
                }]
            },
        ]
    }
]