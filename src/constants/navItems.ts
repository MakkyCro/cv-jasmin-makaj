import { NavItem } from 'src/models/models';

export const navItems: Array<NavItem> = [
    {
        id: 0,
        title: 'nav.introduction',
        icon: 'fas fa-user',
        class: 'tw-whitespace-nowrap',
        routerLink: 'introduction',
        disabled: false,
        separatorBefore: false,
        onlyDrawer: true,
    },
    {
        id: 1,
        title: 'nav.workExperience',
        icon: 'fas fa-briefcase',
        class: 'tw-whitespace-nowrap',
        routerLink: 'work-experience',
        disabled: false,
        separatorBefore: false,
        onlyDrawer: false,
    },
    {
        id: 2,
        title: 'nav.projects',
        icon: 'fas fa-code',
        class: 'tw-whitespace-nowrap',
        routerLink: 'projects',
        disabled: false,
        separatorBefore: false,
        onlyDrawer: false,
    },
    {
        id: 3,
        title: 'nav.education',
        icon: 'fas fa-graduation-cap',
        class: 'tw-whitespace-nowrap',
        routerLink: 'education',
        disabled: false,
        separatorBefore: false,
        onlyDrawer: false,
    },
    {
        id: 4,
        title: 'nav.selectLanguage',
        icon: 'fas fa-language',
        disabled: true,
        separatorBefore: false,
        onlyDrawer: false,
        children: [
            {
                id: 5,
                title: 'languages.croatian',
                icon: 'img:images/croatian.svg',
                click: (emit, self) => {
                    emit('setLanguage', self.value);
                },
                value: 'hr',
                disabled: true,
                separatorBefore: false,
                onlyDrawer: false,
            },
            {
                id: 6,
                title: 'languages.english',
                icon: 'img:images/english.svg',
                click: (emit, self) => {
                    emit('setLanguage', self.value);
                },
                value: 'en',
                disabled: true,
                separatorBefore: false,
                onlyDrawer: false,
            },
            {
                id: 7,
                title: 'languages.german',
                icon: 'img:images/german.svg',
                click: (emit, self) => {
                    emit('setLanguage', self.value);
                },
                value: 'de',
                disabled: true,
                separatorBefore: false,
                onlyDrawer: false,
            },
        ],
    }
]