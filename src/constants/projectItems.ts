import { ProjectList, ProjectTypes, TechnologyTypes } from 'src/models/models';

export const websiteProjectItems: ProjectList = {
    projectType: ProjectTypes.WEBSITE,
    projects: [
        {
            title: 'projects.nonaMare.title',
            description: 'projects.nonaMare.description',
            date: new Date(2023, 5),
            liveUrl: 'https://nonamare.com',
            imageSrc: '/images/projects/nona-mare.png.webp',
            imageSrcSet: '/images/projects/400/nona-mare.png.webp 400w,/images/projects/800/nona-mare.png.webp 2x,/images/projects/1200/nona-mare.png.webp 3x,/images/projects/nona-mare.png.webp 4x',
            technologies: [
                TechnologyTypes.QUASAR,
                TechnologyTypes.VUEJS,
                TechnologyTypes.TYPESCRIPT,
                TechnologyTypes.TAILWINDCSS,
                TechnologyTypes.GITLAB,
                TechnologyTypes.CICD,
                TechnologyTypes.FIGMA,
                TechnologyTypes.VSCODE,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.SCSS,
                TechnologyTypes.LINUX,
            ]
        },
        {
            title: 'projects.benazićVina.title',
            description: 'projects.benazićVina.description',
            date: new Date(2023, 4),
            liveUrl: 'https://benazic.istra.wine',
            imageSrc: '/images/projects/benazic.png.webp',
            imageSrcSet: '/images/projects/400/benazic.png.webp 400w,/images/projects/800/benazic.png.webp 2x,/images/projects/1200/benazic.png.webp 3x,/images/projects/benazic.png.webp 4x',
            technologies: [
                TechnologyTypes.QUASAR,
                TechnologyTypes.VUEJS,
                TechnologyTypes.TYPESCRIPT,
                TechnologyTypes.TAILWINDCSS,
                TechnologyTypes.GITLAB,
                TechnologyTypes.CICD,
                TechnologyTypes.FIGMA,
                TechnologyTypes.VSCODE,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.FONTFORGE,
                TechnologyTypes.SCSS,
                TechnologyTypes.LINUX,
            ]
        },
        {
            title: 'projects.jasminMakajCV.title',
            description: 'projects.jasminMakajCV.description',
            date: new Date(2023, 4),
            liveUrl: 'https://www.jasminm.dev',
            imageSrc: '/images/projects/jasminm.png.webp',
            imageSrcSet: '/images/projects/400/jasminm.png.webp 400w,/images/projects/800/jasminm.png.webp 2x,/images/projects/1200/jasminm.png.webp 3x,/images/projects/jasminm.png.webp 4x',
            projectUrl: 'https://gitlab.com/MakkyCro/cv-jasmin-makaj',
            technologies: [
                TechnologyTypes.QUASAR,
                TechnologyTypes.VUEJS,
                TechnologyTypes.TYPESCRIPT,
                TechnologyTypes.TAILWINDCSS,
                TechnologyTypes.GITLAB,
                TechnologyTypes.CICD,
                TechnologyTypes.FIGMA,
                TechnologyTypes.VSCODE,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.SCSS,
                TechnologyTypes.LINUX,
            ]
        },
        {
            title: 'projects.nakshop.title',
            description: 'projects.nakshop.description',
            date: new Date(2023, 2),
            liveUrl: 'https://nakshop.eu',
            imageSrc: '/images/projects/nakshop.png.webp',
            imageSrcSet: '/images/projects/400/nakshop.png.webp 400w,/images/projects/800/nakshop.png.webp 2x,/images/projects/1200/nakshop.png.webp 3x,/images/projects/nakshop.png.webp 4x',
            technologies: [
                TechnologyTypes.QUASAR,
                TechnologyTypes.VUEJS,
                TechnologyTypes.TYPESCRIPT,
                TechnologyTypes.TAILWINDCSS,
                TechnologyTypes.GITLAB,
                TechnologyTypes.CICD,
                TechnologyTypes.FIGMA,
                TechnologyTypes.VSCODE,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.SCSS,
                TechnologyTypes.LINUX,
            ]
        },
        {
            title: 'projects.luxuryLineAutotepisi.title',
            description: 'projects.luxuryLineAutotepisi.description',
            date: new Date(2022, 4),
            liveUrl: 'http://luxuryline.autotepisi.hr',
            imageSrc: '/images/projects/luxuryline.autotepisi.png.webp',
            imageSrcSet: '/images/projects/400/luxuryline.autotepisi.png.webp 400w,/images/projects/800/luxuryline.autotepisi.png.webp 2x,/images/projects/1200/luxuryline.autotepisi.png.webp 3x,/images/projects/luxuryline.autotepisi.png.webp 4x',
            technologies: [
                TechnologyTypes.QUASAR,
                TechnologyTypes.VUEJS,
                TechnologyTypes.TYPESCRIPT,
                TechnologyTypes.TAILWINDCSS,
                TechnologyTypes.GITLAB,
                TechnologyTypes.CICD,
                TechnologyTypes.FIGMA,
                TechnologyTypes.VSCODE,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.SCSS,
                TechnologyTypes.LINUX,
            ]
        },
        {
            title: 'projects.konobaBokun.title',
            description: 'projects.konobaBokun.description',
            date: new Date(2022, 3),
            liveUrl: 'https://konobabokun.com/en/',
            imageSrc: '/images/projects/konobabokun.png.webp',
            imageSrcSet: '/images/projects/400/konobabokun.png.webp 400w,/images/projects/800/konobabokun.png.webp 2x,/images/projects/1200/konobabokun.png.webp 3x,/images/projects/konobabokun.png.webp 4x',
            technologies: [
                TechnologyTypes.QUASAR,
                TechnologyTypes.VUEJS,
                TechnologyTypes.TYPESCRIPT,
                TechnologyTypes.TAILWINDCSS,
                TechnologyTypes.GITLAB,
                TechnologyTypes.CICD,
                TechnologyTypes.FIGMA,
                TechnologyTypes.VSCODE,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.SCSS,
                TechnologyTypes.LINUX,
            ]
        },
        // {
        //     title: 'projects.dDInnovation.title',
        //     description: 'projects.dDInnovation.description',
        //     date: new Date(2022, 2),
        //     liveUrl: 'https://dd-innovation.hr',
        //     imageSrc: '/images/projects/dd-innovation.png.webp',
        //     imageSrcSet: '/images/projects/400/dd-innovation.png.webp 400w,/images/projects/800/dd-innovation.png.webp 2x,/images/projects/1200/dd-innovation.png.webp 3x,/images/projects/dd-innovation.png.webp 4x',
        //     technologies: [
        //         TechnologyTypes.QUASAR,
        //         TechnologyTypes.VUEJS,
        //         TechnologyTypes.TYPESCRIPT,
        //         TechnologyTypes.TAILWINDCSS,
        //         TechnologyTypes.GITLAB,
        //         TechnologyTypes.CICD,
        //         TechnologyTypes.FIGMA,
        //         TechnologyTypes.VSCODE,
        //         TechnologyTypes.PHOTOSHOP,
        //         TechnologyTypes.SCSS,
        //         TechnologyTypes.LINUX,
        //     ]
        // },
        {
            title: 'projects.sosichWines.title',
            description: 'projects.sosichWines.description',
            date: new Date(2021, 3),
            liveUrl: 'https://sosichwines.hr',
            imageSrc: '/images/projects/sosichwines.png.webp',
            imageSrcSet: '/images/projects/400/sosichwines.png.webp 400w,/images/projects/800/sosichwines.png.webp 2x,/images/projects/1200/sosichwines.png.webp 3x,/images/projects/sosichwines.png.webp 4x',
            technologies: [
                TechnologyTypes.QUASAR,
                TechnologyTypes.VUEJS,
                TechnologyTypes.TYPESCRIPT,
                TechnologyTypes.TAILWINDCSS,
                TechnologyTypes.GITLAB,
                TechnologyTypes.CICD,
                TechnologyTypes.FIGMA,
                TechnologyTypes.VSCODE,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.SCSS,
                TechnologyTypes.LINUX,
            ]
        },

        {
            title: 'projects.istraWine.title',
            description: 'projects.istraWine.description',
            date: new Date(2021, 1),
            liveUrl: 'https://www.istra.wine',
            imageSrc: '/images/projects/istra.wine.png.webp',
            imageSrcSet: '/images/projects/400/istra.wine.png.webp 400w,/images/projects/800/istra.wine.png.webp 2x,/images/projects/1200/istra.wine.png.webp 3x,/images/projects/istra.wine.png.webp 4x',
            technologies: [
                TechnologyTypes.QUASAR,
                TechnologyTypes.VUEJS,
                TechnologyTypes.TYPESCRIPT,
                TechnologyTypes.DROGON,
                TechnologyTypes.CPLUSPLUS,
                TechnologyTypes.TAILWINDCSS,
                TechnologyTypes.GITLAB,
                TechnologyTypes.CICD,
                TechnologyTypes.FIGMA,
                TechnologyTypes.DBEAVER,
                TechnologyTypes.VSCODE,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.SCSS,
                TechnologyTypes.LINUX,
            ]
        },
        {
            title: 'projects.victorySoftware.title',
            description: 'projects.victorySoftware.description',
            date: new Date(2020, 5),
            liveUrl: 'https://victory.hr',
            imageSrc: '/images/projects/victory.png.webp',
            imageSrcSet: '/images/projects/400/victory.png.webp 400w,/images/projects/800/victory.png.webp 2x,/images/projects/1200/victory.png.webp 3x,/images/projects/victory.png.webp 4x',
            technologies: [
                TechnologyTypes.HTML,
                TechnologyTypes.BOOTSTRAP,
                TechnologyTypes.VSCODE,
                TechnologyTypes.SCSS,
            ]
        },
        {
            title: 'projects.exploreCres.title',
            description: 'projects.exploreCres.description',
            date: new Date(2018, 9),
            liveUrl: 'http://test.victory.hr/explorecres.com',
            imageSrc: '/images/projects/cres.png.webp',
            imageSrcSet: '/images/projects/400/cres.png.webp 400w,/images/projects/800/cres.png.webp 2x,/images/projects/1200/cres.png.webp 3x,/images/projects/cres.png.webp 4x',
            technologies: [
                TechnologyTypes.HTML,
                TechnologyTypes.BOOTSTRAP,
                TechnologyTypes.VSCODE,
                TechnologyTypes.SCSS,
            ]
        },
    ]
}


export const gameProjectItems: ProjectList = {
    projectType: ProjectTypes.GAMES,
    projects: [
        {
            title: 'projects.bookworm.title',
            description: 'projects.bookworm.description',
            date: new Date(2019, 8),
            projectUrl: 'https://gitlab.com/MakkyCro/bookworm',
            imageSrc: '/images/projects/bookworm.png.webp',
            imageSrcSet: '/images/projects/400/bookworm.png.webp 400w,/images/projects/800/bookworm.png.webp 2x,/images/projects/1200/bookworm.png.webp 3x,/images/projects/bookworm.png.webp 4x',
            videoUrl: 'https://www.youtube.com/embed/XCoCbBfgTG0',
            technologies: [
                TechnologyTypes.CSHARP,
                TechnologyTypes.DOTNET,
                TechnologyTypes.PYTHON,
                TechnologyTypes.UNITY,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.VISUALSTUDIO,
                TechnologyTypes.VSCODE,
            ]
        },
        {
            title: 'projects.swampAttack.title',
            description: 'projects.swampAttack.description',
            date: new Date(2019, 3),
            projectUrl: 'https://gitlab.com/MakkyCro/gameswampattack',
            imageSrc: '/images/projects/swampattack.png.webp',
            imageSrcSet: '/images/projects/400/swampattack.png.webp 400w,/images/projects/800/swampattack.png.webp 2x,/images/projects/1200/swampattack.png.webp 3x,/images/projects/swampattack.png.webp 4x',
            videoUrl: 'https://www.youtube.com/embed/wGgOvb6M-FQ',
            technologies: [
                TechnologyTypes.CSHARP,
                TechnologyTypes.DOTNET,
                TechnologyTypes.UNITY,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.VISUALSTUDIO,
                TechnologyTypes.VSCODE,
            ]
        },
        {
            title: 'projects.skullKnight.title',
            description: 'projects.skullKnight.description',
            date: new Date(2018, 1),
            projectUrl: 'https://gitlab.com/MakkyCro/skullknight',
            imageSrc: '/images/projects/skullknight.png.webp',
            imageSrcSet: '/images/projects/400/skullknight.png.webp 400w,/images/projects/800/skullknight.png.webp 2x,/images/projects/1200/skullknight.png.webp 3x,/images/projects/skullknight.png.webp 4x',
            videoUrl: '',
            technologies: [
                TechnologyTypes.CSHARP,
                TechnologyTypes.DOTNET,
                TechnologyTypes.UNITY,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.VISUALSTUDIO,
                TechnologyTypes.VSCODE,
                TechnologyTypes.BLENDER
            ]
        },
    ]
}


export const miscProjectItems: ProjectList = {
    projectType: ProjectTypes.MISC,
    projects: [
        {
            title: 'projects.programForHospitalityFiskalVictory.title',
            description: 'projects.programForHospitalityFiskalVictory.description',
            date: new Date(2021, 9),
            projectUrl: 'https://gitlab.com/MakkyCro/fiskalvictory',
            imageSrc: '/images/projects/fiskalvictory.png.webp',
            imageSrcSet: '/images/projects/400/fiskalvictory.png.webp 400w,/images/projects/800/fiskalvictory.png.webp 2x,/images/projects/1200/fiskalvictory.png.webp 3x,/images/projects/fiskalvictory.png.webp 4x',
            technologies: [
                TechnologyTypes.CSHARP,
                TechnologyTypes.DOTNETCORE,
                TechnologyTypes.PYTHON,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.FIGMA,
                TechnologyTypes.AWS,
                TechnologyTypes.DIGITALOCEAN,
                TechnologyTypes.NGINX,
                TechnologyTypes.LINUX,
                TechnologyTypes.MARIADB,
                TechnologyTypes.SQLITE,
                TechnologyTypes.FONTFORGE,
                TechnologyTypes.VSCODE,
                TechnologyTypes.VISUALSTUDIO,
                TechnologyTypes.MKDOCS,
                TechnologyTypes.PUTTY,
                TechnologyTypes.GITLAB,
                TechnologyTypes.GITLABRUNNER,
                TechnologyTypes.CICD,
            ]
        },
        {
            title: 'projects.n1NewsPortalArticleAnalyzer.title',
            description: 'projects.n1NewsPortalArticleAnalyzer.description',
            date: new Date(2020, 9),
            projectUrl: 'https://gitlab.com/MakkyCro/n1-news-portal-scraping',
            imageSrc: '/images/projects/n1articles.png.webp',
            imageSrcSet: '/images/projects/400/n1articles.png.webp 400w,/images/projects/800/n1articles.png.webp 2x,/images/projects/1200/n1articles.png.webp 3x,/images/projects/n1articles.png.webp 4x',
            technologies: [
                TechnologyTypes.PYTHON,
                TechnologyTypes.SELENIUM,
            ]
        },
        {
            title: 'projects.emailSenderToApartmentOwners.title',
            description: 'projects.emailSenderToApartmentOwners.description',
            date: new Date(2020, 6),
            projectUrl: 'https://gitlab.com/MakkyCro/appartmentfinder',
            imageSrc: '/images/projects/emailSender.png.webp',
            imageSrcSet: '/images/projects/400/emailSender.png.webp 400w,/images/projects/800/emailSender.png.webp 2x,/images/projects/1200/emailSender.png.webp 3x,/images/projects/emailSender.png.webp 4x',
            technologies: [
                TechnologyTypes.PYTHON,
                TechnologyTypes.BEAUTIFULSOUP,
                TechnologyTypes.JINJA,
            ]
        }, {
            title: 'projects.effectsOfCOVID19VirusOnNumberOfPlayersOnSteamPlatform.title',
            description: 'projects.effectsOfCOVID19VirusOnNumberOfPlayersOnSteamPlatform.description',
            date: new Date(2020, 4),
            projectUrl: 'https://gitlab.com/MakkyCro/effects-of-covid-19-on-steam-platform',
            imageSrc: '/images/projects/steamCovid.png.webp',
            imageSrcSet: '/images/projects/400/steamCovid.png.webp 400w,/images/projects/800/steamCovid.png.webp 2x,/images/projects/1200/steamCovid.png.webp 3x,/images/projects/steamCovid.png.webp 4x',
            technologies: [
                TechnologyTypes.PYTHON,
                TechnologyTypes.SELENIUM,
            ]
        },
        {
            title: 'projects.thumbnailGeneratorForListOfProducts.title',
            description: 'projects.thumbnailGeneratorForListOfProducts.description',
            date: new Date(2019, 6),
            videoUrl: 'https://www.youtube.com/embed/o-6mmpg3tO8',
            imageSrc: '/images/projects/thumbnailGenerator.png.webp',
            imageSrcSet: '/images/projects/400/thumbnailGenerator.png.webp 400w,/images/projects/800/thumbnailGenerator.png.webp 2x,/images/projects/1200/thumbnailGenerator.png.webp 3x,/images/projects/thumbnailGenerator.png.webp 4x',
            technologies: [
                TechnologyTypes.CSHARP,
                TechnologyTypes.VISUALSTUDIO,
                TechnologyTypes.DOTNET,
            ]
        },
        {
            title: 'projects.songScrapingToolAndAutomaticDownloader.title',
            description: 'projects.songScrapingToolAndAutomaticDownloader.description',
            date: new Date(2019, 5),
            projectUrl: 'https://gitlab.com/MakkyCro/music-scraper',
            imageSrc: '/images/projects/musicScrapper.png.webp',
            imageSrcSet: '/images/projects/400/musicScrapper.png.webp 400w,/images/projects/800/musicScrapper.png.webp 2x,/images/projects/1200/musicScrapper.png.webp 3x,/images/projects/musicScrapper.png.webp 4x',
            technologies: [
                TechnologyTypes.PYTHON,
                TechnologyTypes.BEAUTIFULSOUP,
                TechnologyTypes.VSCODE,
            ]
        },
        {
            title: 'projects.hexapodProgram.title',
            description: 'projects.hexapodProgram.description',
            date: new Date(2015, 5),
            videoUrl: 'https://www.youtube.com/embed/q2thpDE45zY',
            imageSrc: '/images/projects/hexapod.png.webp',
            imageSrcSet: '/images/projects/400/hexapod.png.webp 400w,/images/projects/800/hexapod.png.webp 2x,/images/projects/1200/hexapod.png.webp 3x,/images/projects/hexapod.png.webp 4x',
            technologies: [
                TechnologyTypes.ARDUINO,
                TechnologyTypes.C,
            ]
        },
        {
            title: 'projects.scriptGeneratorForCounterStrikeGlobalOffensive.title',
            description: 'projects.scriptGeneratorForCounterStrikeGlobalOffensive.description',
            date: new Date(2015, 5),
            projectUrl: 'https://gamebanana.com/tools/5916',
            imageSrc: '/images/projects/csgo.png.webp',
            imageSrcSet: '/images/projects/400/csgo.png.webp 400w,/images/projects/800/csgo.png.webp 2x,/images/projects/1200/csgo.png.webp 3x,/images/projects/csgo.png.webp 4x',
            technologies: [
                TechnologyTypes.CSHARP,
                TechnologyTypes.DOTNET,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.VISUALSTUDIO,
            ]
        },
        {
            title: 'projects.scriptGeneratorForCounterStrikeSource.title',
            description: 'projects.scriptGeneratorForCounterStrikeSource.description',
            date: new Date(2013, 5),
            projectUrl: 'https://gamebanana.com/tools/5490',
            imageSrc: '/images/projects/css.png.webp',
            imageSrcSet: '/images/projects/400/css.png.webp 400w,/images/projects/800/css.png.webp 2x,/images/projects/1200/css.png.webp 3x,/images/projects/css.png.webp 4x',
            technologies: [
                TechnologyTypes.CSHARP,
                TechnologyTypes.DOTNET,
                TechnologyTypes.PHOTOSHOP,
                TechnologyTypes.VISUALSTUDIO,
            ]
        },
    ]
}