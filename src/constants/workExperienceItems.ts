import { TimelinePeriod } from 'src/models/models';

export const workExperienceList: TimelinePeriod[] = [
    {
        title: 'nav.workExperience',
        events: [
            {
                date: new Date(2023, 2),
                dateUntil: new Date(2023, 4),
                title: 'workExperience.benazic.title',
                location: 'workExperience.benazic.location',
                locationUrl: 'https://www.istra.wine',
                content: 'workExperience.benazic.content',
                buttons: [{
                    title: 'nav.openWebsite',
                    routerLink: 'https://benazic.istra.wine/',
                    icon: 'fas fa-globe'
                }]
            },
            {
                date: new Date(2021, 10),
                dateUntil: 'untilToday',
                title: 'workExperience.crc.title',
                location: 'workExperience.crc.location',
                locationUrl: 'https://crc.ag',
                content: 'workExperience.crc.content',
            },
            {
                date: new Date(2019, 9),
                dateUntil: 'untilToday',
                title: 'workExperience.iw.title',
                location: 'workExperience.iw.location',
                locationUrl: 'https://www.istra.wine',
                content: 'workExperience.iw.content',
            },
            {
                date: new Date(2018, 8),
                dateUntil: new Date(2020, 5),
                title: 'workExperience.uni.title',
                location: 'workExperience.uni.location',
                locationUrl: 'https://www.inf.uniri.hr',
                content: 'workExperience.uni.content',
            },
            {
                date: new Date(2017, 8),
                title: 'workExperience.frff.title',
                location: 'workExperience.frff.location',
                content: 'workExperience.frff.content',
            },
            {
                date: new Date(2016, 3),
                title: 'workExperience.dignitas1.title',
                location: 'workExperience.dignitas1.location',
                content: 'workExperience.dignitas1.content',
            },
        ]

    }
]